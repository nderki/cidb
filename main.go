package main

import (
  "database/sql"
  _ "github.com/go-sql-driver/mysql"
  "fmt"
  "os"
)

func main(){
    MYSQL_HOST:=os.Getenv("MYSQL_HOST")
    MYSQL_USER:=os.Getenv("MYSQL_USER")
    MYSQL_ROOT_PASSWORD:=os.Getenv("MYSQL_ROOT_PASSWORD")
    MYSQL_TRANSPORT:=os.Getenv("MYSQL_TRANSPORT")
    MYSQL_ADDRESS:=os.Getenv("MYSQL_ADDRESS")
    MYSQL_DATABASE:=os.Getenv("MYSQL_DATABASE")

    fmt.Println(MYSQL_HOST)
    fmt.Println(MYSQL_USER)
    fmt.Println(MYSQL_ROOT_PASSWORD)
    fmt.Println(MYSQL_TRANSPORT)
    fmt.Println(MYSQL_ADDRESS)
    fmt.Println(MYSQL_DATABASE)
    db, err:=sql.Open(MYSQL_HOST, MYSQL_USER+":"+MYSQL_ROOT_PASSWORD+"@"+MYSQL_TRANSPORT+"("+MYSQL_ADDRESS+")/")
    if err!=nil{fmt.Println(err)}
    defer db.Close()

    _, err=db.Exec("CREATE DATABASE IF NOT EXISTS storage;")
    if err!=nil{fmt.Println(err)}

    _, err=db.Exec("USE "+MYSQL_DATABASE)
    if err!=nil{fmt.Println(err)}

    stmt, err:=db.Prepare("CREATE TABLE IF NOT EXISTS transactions(id INT NOT NULL, content_hash VARCHAR(46) NOT NULL);")
    if err!=nil{fmt.Println(err)}
    _, err=stmt.Exec()
    if err!=nil{fmt.Println(err)}

    _, err=db.Exec(`INSERT INTO transactions (id, content_hash) values(?, ?);`, 2, "Qmnn643yg4w9acw")
	  if err!=nil{fmt.Println(err)}

	  rows, err:=db.Query("SELECT * FROM transactions;")
    if err!=nil{fmt.Println(err)}
    defer rows.Close()
    var (
        id int
        hash string
    )
    for rows.Next() {
        err=rows.Scan(&id, &hash)
        if err!=nil{fmt.Println(err)}
        fmt.Println(id, hash)
    }
}
